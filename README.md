# hello

How to push an image to gitlab.

First go to `https://gitlab.com/alexcb/hello/container_registry`, which should show URLs such as:

    docker login registry.gitlab.com
    docker build -t registry.gitlab.com/alexcb/hello .
    docker push registry.gitlab.com/alexcb/hello


Let's try logging in with:

    alex@mah:~/gl/alexcb/hello$ docker login registry.gitlab.com
    Username: alexcb
    Password: 
    WARNING! Your password will be stored unencrypted in /home/alex/.docker/config.json.
    Configure a credential helper to remove this warning. See
    https://docs.docker.com/engine/reference/commandline/login/#credentials-store
    
    Login Succeeded


