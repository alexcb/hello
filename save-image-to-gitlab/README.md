# Example  of pushing image to gitlab repo

## Step 1, login to gitlab registry.

First go to `https://gitlab.com/alexcb/hello/container_registry`, which should show commands such as:
    
        docker login registry.gitlab.com
        docker build -t registry.gitlab.com/alexcb/hello .
        docker push registry.gitlab.com/alexcb/hello

Copy and paste the docker login command:

    $ docker login registry.gitlab.com
    Username: alexcb
    Password: 
    WARNING! Your password will be stored unencrypted in /home/alex/.docker/config.json.
    Configure a credential helper to remove this warning. See
    https://docs.docker.com/engine/reference/commandline/login/#credentials-store
    
    Login Succeeded

## Step 2, push the image


    $ earthly --push +hello 
               buildkitd | Found buildkit daemon as docker container (earthly-buildkitd)
           alpine:latest | --> Load metadata linux/amd64
                  +hello | --> FROM alpine:latest
                  +hello | [██████████] resolve docker.io/library/alpine:latest@sha256:a75afd8b57e7f34e4dad8d65e2c7ba2e1975c795ce1ee22fa34f8cf46f96a3be ... 100%
                  output | --> exporting outputs
                  output | [██████████] exporting layers ... 100%
                  output | [██████████] exporting manifest sha256:e05f72bb317a154e3cdcd215b213602787dace60a792b665d942a187b80b2070 ... 100%
                  output | [██████████] exporting config sha256:35acbbd1bd10cecbe9080365fc215d231f38c1956c1b69c5445000e963adfefd ... 100%
                  output | [          ] pushing layers ... 0%
                 ongoing | output (a step started 5 seconds ago)
                  output | [██████████] pushing layers ... 100%
                  output | [██████████] pushing manifest for registry.gitlab.com/alexcb/hello:latest ... 100%
                  output | [██████████] sending tarballs ... 100%
    ================================ SUCCESS [main] ================================
    2aa01d95c08c: Loading layer [==================================================>]     193B/193B
    The image registry.gitlab.com/alexcb/hello:latest already exists, renaming the old one with ID sha256:37bd631c5c7bb0b9d8de6113fe550b448d80df6d81e61b847846524665d93db4 to empty string
    Loaded image: registry.gitlab.com/alexcb/hello:latest
    =============================== SUCCESS [--push] ===============================
                  +hello | Image gitlab.com/alexcb/hello:master+hello as registry.gitlab.com/alexcb/hello:latest (pushed)
    

## Step 3, try running the image

First lets ensure we delete the locally cached image:

    $ docker rmi registry.gitlab.com/alexcb/hello:latest
    Untagged: registry.gitlab.com/alexcb/hello:latest
    Deleted: sha256:35acbbd1bd10cecbe9080365fc215d231f38c1956c1b69c5445000e963adfefd
    Deleted: sha256:9a05d3bef3786f75d7b144339e50b964f4d6f8bb38c0505516c5f5093b30044d

Next lets try running it:

    $ docker run --rm registry.gitlab.com/alexcb/hello:latest
    Unable to find image 'registry.gitlab.com/alexcb/hello:latest' locally
    latest: Pulling from alexcb/hello
    ba3557a56b15: Already exists 
    26773cd5c1b7: Pull complete 
    Digest: sha256:e05f72bb317a154e3cdcd215b213602787dace60a792b665d942a187b80b2070
    Status: Downloaded newer image for registry.gitlab.com/alexcb/hello:latest
    hello world
